var matchdep = require('matchdep');

module.exports = function(grunt) {

  matchdep.filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    browserify: {
      dist: {
        files: { 'public/application.js': ['client/javascripts/index.js'] }
      },
      options: {
        shim: {
          jquery: {
            path: 'bower_components/jquery/jquery.js',
            exports: '$'
          },
          modernizr: {
            path: 'bower_components/modernizr/modernizr.js',
            exports: 'modernizr'
          },
          foundation: {
            path: 'bower_components/foundation/js/foundation.js',
            exports: 'foundation'
          }
        }
      }
    },

    sass: {
      dist: {
        files: {
          'public/application.css': ['client/scss/main.scss']
        }
      }
    },

    nodemon: {
      dev: {}
    },

    watch: {
      browserify: {
        files: ['client/javascripts/*.js'],
        tasks: ['browserify']
      },
      sass: {
        files: ['client/scss/*.scss'],
        tasks: ['sass']
      }
    },

    concurrent: {
      main: {
        tasks: ['nodemon', 'watch']
      },
      options: {
        logConcurrentOutput: true
      }
    }
  });

  grunt.registerTask('server', ['concurrent']);
};
