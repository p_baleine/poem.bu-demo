var modernizr = require('modernizr'),
    $ = require('jquery');

// expose `jQuery` for foundation.

window.jQuery = window.$ = $;

var foundation = require('foundation');

$(document).foundation();
