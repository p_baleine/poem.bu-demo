var Promise = require('bluebird'),
    fs = Promise.promisifyAll(require('fs')),
    path = require('path'),
    express = require('express'),

    app = express();

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.static(__dirname + '/public'));

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.get('*', function(req, res, next) {
  var filename = req.url.replace(/\/(.+)/, '$1');

  fs.statAsync(path.resolve(app.get('views') + '/' + filename + '.jade'))
    .then(function() {
      res.render(filename);
    }, function() {
      res.statusCode = 404;
      next('file not found');
    });
});

if (!module.parent) {
  app.listen(3000);
  console.log('start on port %s', 3000);
}
